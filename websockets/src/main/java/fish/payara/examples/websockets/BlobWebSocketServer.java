/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fish.payara.examples.websockets;

import java.io.ByteArrayOutputStream;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author SITOU
 */
@ApplicationScoped
@ServerEndpoint("/blob")
public class BlobWebSocketServer {
    
    private static final Logger logger = Logger.getLogger(BlobWebSocketServer.class.getName());

    // バッファ
    private ByteArrayOutputStream baos = null;

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        // 何もしません。
    }

    @OnMessage
    public void onMessage(byte[] bs, boolean last, Session session) {
        try {
            System.out.println(session.getId() + "/" + bs.length + "/" + last);
            // バッファ存在チェック
            if (baos == null) {
                // バッファ生成
                baos = new ByteArrayOutputStream();
            }
            // 書き込み
            baos.write(bs);
            baos.flush();
            // 最後であるか判定
            if (last) {
                // 最後なので、byte配列化
                byte[] data = baos.toByteArray();
                // とりあえず、文字列にして出力
                System.out.println(new String(data));
                // バッファ後処理
                baos.close();
                baos = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        // 何もしません。
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        // 何もしません。
    }
}
